//
//  ProfileVC.swift
//  Doctor
//
//  Created by Jaydeep on 03/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage

class ProfileVC: UIViewController {
    
    //MARK:- Variable Decalration
    
    var dictUserDetail = JSON()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblMyLocum: UILabel!
    @IBOutlet weak var lblMyBooking: UILabel!
    
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        getUserProfileDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithSideMenu(strTitle: "Profile")
    }

    //MARK:- Setup
    
    func setupProfileData(){
        imgProfile.sd_setImage(with: dictUserDetail["profile_url"].url , placeholderImage:nil, options: .lowPriority, completed: nil)
        imgProfile.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        
        lblUsername.text = dictUserDetail["user_name"].stringValue
        lblUserEmail.text = dictUserDetail["email_id"].stringValue
        lblMobileNumber.text = "\(dictUserDetail["country_code"].stringValue)\(dictUserDetail["contact_number"].stringValue)"
        lblMyBooking.text = dictUserDetail["my_booking"].stringValue
        lblMyLocum.text = dictUserDetail["my_locum"].stringValue
    }
}

///MARK:- Action Zome
extension ProfileVC {
    
    @IBAction func btnEditProfileAction(_ sender:UIButton){
       let obj = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        obj.dictUserDetail = dictUserDetail
        obj.handlerEdirProfile = {[weak self] in
            self?.getUserProfileDetail()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnChangePasswordAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Service

extension ProfileVC {
    func getUserProfileDetail()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kGetProfile)"
            
            print("URL: \(url)")
            
            let param =  ["dc_user_id":getUserDetail("dc_user_id")
                         ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        self.dictUserDetail = json["data"]
                        self.setupProfileData()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserDetail"), object: nil)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}
