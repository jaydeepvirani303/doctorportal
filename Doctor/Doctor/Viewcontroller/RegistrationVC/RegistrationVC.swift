//
//  RegistrationVC.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import GooglePlaces
import CoreLocation
import FirebaseMessaging

class RegistrationVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var btnDoctorRegistrationOutlet: CustomButton!
    @IBOutlet weak var btnClinicRegistrationOutlet: CustomButton!
    @IBOutlet weak var vwClinic: UIView!
    @IBOutlet weak var vwClinicAddress: UIView!
    @IBOutlet weak var vwRegistration: UIView!
    @IBOutlet weak var imgDoctorAnchor: UIImageView!    
    @IBOutlet weak var imgClinicAnchor: UIImageView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtClinicName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var txtRegistrationNumber: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var vwPassword: UIView!
    @IBOutlet weak var vwConfirmPassword: UIView!
    
    //MARK:- Variable Declaration
    
    var clinicLatLong = CLLocationCoordinate2D()
    var isComeFromSocial = Bool()
    var dictData = JSON()
    
    //MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showDoctorRegistrationView()
        
        if isComeFromSocial {
            [vwConfirmPassword,vwPassword].forEach { (vw) in
                vw?.isHidden = true
            }
            self.txtFullName.text = dictData["name"].stringValue
            self.txtEmailAddress.text = dictData["email"].stringValue
        }
        
        [txtFullName,txtClinicName,txtPhoneNumber,txtCountryCode,txtRegistrationNumber,txtEmailAddress,txtPassword,txtConfirmPassword].forEach { (txtField) in
            txtField?.delegate = self
        }
        
        clinicLatLong = kUserCurrentLocation
        
        appDelegate.delegate = self
        appDelegate.locationManager.startUpdatingLocation()
        
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        
    }
}

//MARK:- get Current Location Delegate

extension RegistrationVC:CurrentLocationDelegate {
    func didUpdateLocation(lat: Double?, lon: Double?) {
        
        if lat == nil && lon == nil {
            return
        }
        clinicLatLong = CLLocationCoordinate2D(latitude: lat ?? 20.5937, longitude: lon ?? 78.9629)
//        showLoader()
        let strLocation = getAddressFromLatLon(latitude: clinicLatLong.latitude, longitude: clinicLatLong.longitude) { (currentAddress) in
            self.stopLoader()
            self.txtViewAddress.text = currentAddress
            self.lblPlaceholder.isHidden = self.txtViewAddress.text.isEmpty ? false : true
            self.txtCountryCode.text = "+" + strCountryDialingCode
        }
    }
}

//MARK:- Priavate Zone

extension RegistrationVC {
    func showDoctorRegistrationView(){
        [vwClinic,vwClinicAddress].forEach { (vw) in
            vw?.isHidden = true
        }
        
        [vwRegistration].forEach { (vw) in
            vw?.isHidden = false
        }
        
        txtRegistrationNumber.placeholder = "Enter Doctor's Registration No"
        
        imgClinicAnchor.isHidden = true
        imgDoctorAnchor.isHidden = false
        
        btnDoctorRegistrationOutlet.isSelected = true
        btnClinicRegistrationOutlet.isSelected = false
        
        btnClinicRegistrationOutlet.backgroundColor = .clear
        btnDoctorRegistrationOutlet.backgroundColor = .white
        
        btnClinicRegistrationOutlet.borderWidth = 1
        btnClinicRegistrationOutlet.borderColor = UIColor.white
        btnClinicRegistrationOutlet.cornerRadius = 5
        
    }
    
    func showClinicRegistrationView(){
        [vwClinic,vwClinicAddress].forEach { (vw) in
            vw?.isHidden = false
        }
        
        [vwRegistration].forEach { (vw) in
            vw?.isHidden = false
        }
        
        txtRegistrationNumber.placeholder = "Enter Clinic/Hosp. Registration No"
        
        btnDoctorRegistrationOutlet.isSelected = false
        btnClinicRegistrationOutlet.isSelected = true
        
        imgClinicAnchor.isHidden = false
        imgDoctorAnchor.isHidden = true
        
        btnClinicRegistrationOutlet.backgroundColor = .white
        btnDoctorRegistrationOutlet.backgroundColor = .clear
        
        btnDoctorRegistrationOutlet.borderWidth = 1
        btnDoctorRegistrationOutlet.borderColor = UIColor.white
        btnDoctorRegistrationOutlet.cornerRadius = 5
    }
}

//MARK:- Action ZOne

extension RegistrationVC {
    
    @IBAction func btnDoctorRegistrationAction(_ sender:UIButton){
        showDoctorRegistrationView()
    }
    
    @IBAction func btnClinicRegistrationAction(_ sender:UIButton){
       showClinicRegistrationView()
    }
    
    @IBAction func btnSignupAction(_ sender:UIButton){
        
        objUser = User()
        objUser.strFullName = self.txtFullName.text ?? ""
        objUser.strMobileNumber = self.txtPhoneNumber.text ?? ""
        objUser.strRegistrationNo = self.txtRegistrationNumber.text ?? ""
        objUser.strEmailAddress = self.txtEmailAddress.text ?? ""
        objUser.strPassword = self.txtPassword.text ?? ""
        objUser.strConfirmPassword = self.txtConfirmPassword.text ?? ""
        objUser.strClinicName  = self.txtClinicName.text ?? ""
        objUser.strClinicAddress  = self.txtViewAddress.text ?? ""
        objUser.strCountryCode = self.txtCountryCode.text ?? ""
       
        if btnDoctorRegistrationOutlet.isSelected {
            if isComeFromSocial {
                if objUser.isDoctorRegistrationWithSocialAccount()
                {
                    isDoctorRegistration()
                }
                else
                {
                    makeToast(message: objUser.strValidationMessage)
                }
            } else{
                if objUser.isDoctorRegistration()
                {
                    isDoctorRegistration()
                }
                else
                {
                    makeToast(message: objUser.strValidationMessage)
                }
            }
            
        } else{
            if isComeFromSocial {
                if objUser.isClinicRegistrationWithSocialAccount()
                {
                    isDoctorRegistration()
                }
                else
                {
                    makeToast(message: objUser.strValidationMessage)
                }
            } else {
                if objUser.isClinicRegistration()
                {
                    isDoctorRegistration()
                }
                else
                {
                    makeToast(message: objUser.strValidationMessage)
                }
            }
        }

    }
    
    @IBAction func btnCurrentLocation(_ sender:UIButton){
        appDelegate.setUpQuickLocationUpdate()
    }
    
    @IBAction func btnTermsConditionAction(_ sender:UIButton){
        
        UIApplication.shared.open(URL(string: "http://www.doctorportal.info")!, options: [:], completionHandler: nil)
        
    }
}

//MARK:- UITextfield Delegate

extension RegistrationVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtCountryCode {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == txtPhoneNumber {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}


//MARK:- UITextview Delegate

extension RegistrationVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.lblPlaceholder.isHidden = false
        } else {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        setupGooglePlacePicker()
        return false
    }
}

//MARK:- Google Place Picker

extension RegistrationVC:GMSAutocompleteViewControllerDelegate {
    
    func setupGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.all.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress)")
        
        clinicLatLong = place.coordinate
        
        self.txtViewAddress.text = place.formattedAddress ?? ""
        self.lblPlaceholder.isHidden = self.txtViewAddress.text == "" ? false : true
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

}

//MARK:- Service

extension RegistrationVC
{
    func isDoctorRegistration()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            var url = String()
            if isComeFromSocial {
                url = "\(kBasicURL)\(kSocialAccount)"
            } else {
                url = "\(kBasicURL)\(kRegistration)"
            }
            
            print("URL: \(url)")
            
            var param =  ["email_id":objUser.strEmailAddress,
                          "device_token":Messaging.messaging().fcmToken ?? "",
                          "type":self.btnDoctorRegistrationOutlet.isSelected == true ? "Doctor" : "Clinic",
                          "device_type":strDeviceType,
                          "user_name":objUser.strFullName.trimmingCharacters(in: .whitespacesAndNewlines),
                          "contact_number":objUser.strMobileNumber,
                          "country_code":objUser.strCountryCode,
                          "registration_number":objUser.strRegistrationNo
                          ]
           
            
            if self.btnClinicRegistrationOutlet.isSelected == true{
                param["lat"] = "\(clinicLatLong.latitude)"
                param["lng"] = "\(clinicLatLong.longitude)"
                param["clinic_address"] = self.txtViewAddress.text ?? ""
                param["clinic_name"] = objUser.strClinicName
            } else {
                param["lat"] = "\(kUserCurrentLocation.latitude)"
                param["lng"] = "\(kUserCurrentLocation.longitude)"
//                param["registration_number"] = ""
            }
            
            if isComeFromSocial {
                param["profile_logo"] = dictData["profile_image"].stringValue
                param["social_id"] = dictData["id"].stringValue
                param["social_type"] = dictData["social_type"].stringValue
            } else {
                param["password"] = objUser.strPassword
                param["profile_logo"] = ""
            }
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {                        
                        
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
                        self.navigateUserWithLG(obj: obj)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
}
