//
//  DoctoreDetailVC.swift
//  Doctor
//
//  Created by Jaydeep on 04/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage

class DoctoreDetailVC: UIViewController {
    
    //MARK:- Variablde Declaration
    
    var dictDoctorDetail = JSON()
    var handlerRefreshApplicant:() -> Void = {}
    var arrRate:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwAllGiveRate: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var txtDoctorDescription: UITextView!
    @IBOutlet weak var lblTotalExperienced: UILabel!
    @IBOutlet weak var lblTotalCompleted: UILabel!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var btnApplyOutlet:UIButton!
    @IBOutlet weak var vwPuncutualityRate: HCSStarRatingView!
    @IBOutlet weak var vwClinicJudgementRate: HCSStarRatingView!
    @IBOutlet weak var vwPatientFriendly: HCSStarRatingView!
    @IBOutlet weak var tblRate: UITableView!
    @IBOutlet weak var heightOfTblRate: NSLayoutConstraint!
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithBackButton(strTitle: "Doctor Details")
        tblRate.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblRate.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            self.heightOfTblRate.constant  = tblRate.contentSize.height
        }
    }
    
    //MARK:- SetupUI
    
    func setupUI(){
        imgProfile.sd_setImage(with: dictDoctorDetail["profile_url"].url , placeholderImage:nil, options: .lowPriority, completed: nil)
        lblDoctorName.text = dictDoctorDetail["user_name"].stringValue
//        lblMobileNumber.text = "\(dictDoctorDetail["country_code"].stringValue)\(dictDoctorDetail["contact_number"].stringValue)"
        lblTotalExperienced.text = "\(dictDoctorDetail["year_experience"].stringValue)"
        lblTotalCompleted.text = "\(dictDoctorDetail["total_gigs"].stringValue)"
        txtDoctorDescription.text = dictDoctorDetail["bio"].stringValue
        vwRating.value = CGFloat(dictDoctorDetail["rating_average"].floatValue)
        
        btnApplyOutlet.isHidden = true
        vwAllGiveRate.isHidden = true
        self.lblMobileNumber.isHidden = false
        
        if dictDoctorDetail["is_review_enable_for_doctor_user"].stringValue == "1" {
            vwAllGiveRate.isHidden = false
            arrRate = dictDoctorDetail["review_list"].arrayValue
            self.tblRate.reloadData()
        }
    }
}

//MARK:- Action Zone

extension DoctoreDetailVC {
    
    @IBAction func btnApplyAction(_ sender : UIButton){
        if dictDoctorDetail["is_hire"].stringValue == "1" {
            return
        }
        hireLocum()
    }
    
    @IBAction func btnMobileNumberAction(_ sender:UIButton){
        let alert = UIAlertController(title: APP_NAME, message: "Please select an option for call", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Phone", style: .default , handler:{ (UIAlertAction)in
            let mobileNumber = "\(self.dictDoctorDetail["country_code"].stringValue)\(self.dictDoctorDetail["contact_number"].stringValue)"
            guard let number = URL(string: "tel://\(mobileNumber)") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(number)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "WhatsApp", style: .default , handler:{ (UIAlertAction)in
            let mobileNumber = "\(self.dictDoctorDetail["country_code"].stringValue)\(self.dictDoctorDetail["contact_number"].stringValue)"
            guard let number = URL(string: "whatsapp://send?phone=\(mobileNumber)") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(number)
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func btnSubmitRateAction(_ sender:UIButton){
        for i in 0..<arrRate.count{
            var dict = arrRate[i]
            if let cell = tblRate.cellForRow(at: IndexPath(row: i, section: 0)) as? RateCell {
                dict["rating"] = JSON(cell.vwRate.value)
            }
            arrRate[i] = dict
        }
        rateToLocum()
    }
}

//MARK:- Tableviewd Delegate and Datasource

extension DoctoreDetailVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell") as! RateCell
        let dict = arrRate[indexPath.row]
        cell.lblRatingName.text = dict["title"].stringValue
        return cell
    }
   
}

//MARK:- API

extension DoctoreDetailVC {
    func hireLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kLocumHire)"
            
            print("URL: \(url)")
            
            let param =  ["dc_locum_request_id":dictDoctorDetail["dc_locum_request_id"].stringValue
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.handlerRefreshApplicant()
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func rateToLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kAllRating)"
            
            print("URL: \(url)")
            var arrGiveRate:[JSON] = []
            for i in 0..<arrRate.count {
                var dict = JSON()
                dict["dc_review_list_id"].stringValue = arrRate[i]["dc_review_list_id"].stringValue
                dict["rating"].stringValue = arrRate[i]["rating"].stringValue
                arrGiveRate.append(dict)
            }
            
            let param:[String:Any] =  ["dc_user_id":getUserDetail("dc_user_id"),
                                       "review_type":dictDoctorDetail["review_type"].stringValue,
                                       "global_id":dictDoctorDetail["global_id"].stringValue,
                                       "review_list":arrGiveRate]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.vwAllGiveRate.isHidden = true
                        self.handlerRefreshApplicant()
                        makeToast(message: json["message"].stringValue)
                        self.navigationController?.popViewController(animated: true)                        
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}
