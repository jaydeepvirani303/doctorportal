//
//  SideMenuVC.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblInfoMessage: UILabel!
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var viewOfOuter: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    
    //MARK:- View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(viewDidAppear), name: NSNotification.Name(rawValue: "FadeInEffect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillDisappear), name: NSNotification.Name(rawValue: "FadeOutEffect"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillAppear), name: NSNotification.Name(rawValue: "updateUserDetail"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgProfile.sd_setImage(with: URL(string: getUserDetail("profile_url")) , placeholderImage:nil, options: .lowPriority, completed: nil)
        imgProfile.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        
        lblUserName.text = getUserDetail("user_name")
        
        if getUserDetail("type") == checkDoctorClinic.doctor.rawValue{
            lblInfoMessage.text = "Currently logged as Doctor"
        } else {
            lblInfoMessage.text = "Currently logged as Clinic/hospital"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        fadeInEffect()
//        self.perform(#selector(fadeInEffect), with: nil, afterDelay: 0.1)
    }
    
    @objc func fadeInEffect(){
        UIView.transition(with: self.viewOfOuter, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            self.viewOfOuter.backgroundColor = UIColor.lightGray
            self.viewOfOuter.alpha = 0.30
            self.viewOfOuter.isHidden = false
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.viewOfOuter.alpha = 0
        self.viewOfOuter.isHidden = true
        viewOfOuter.fadeOut()
    }

}

//MARK:- Action Zone

extension SideMenuVC {
    
    @IBAction func btnHomeAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
        self.navigateUserWithLG(obj: obj)
    }
    
    @IBAction func btnCalenderAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        self.navigateUserWithLG(obj: obj)
    }
    
    @IBAction func btnProfileAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigateUserWithLG(obj: obj)
    }
    
    @IBAction func btnLogoutAction(_ sender:UIButton){
        let alertController = UIAlertController(title: APP_NAME, message:"Do you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            Defaults.removeObject(forKey: "userDetail")
            Defaults.synchronize()
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigateUserWithLG(obj: obj)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
