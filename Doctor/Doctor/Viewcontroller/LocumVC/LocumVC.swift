//
//  LocumVC.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    var index: Int!
    var dictData : JSON
    
    init(position: CLLocationCoordinate2D, name: String, dict: JSON) {
        self.position = position
        self.name = name
        self.dictData = dict
    }
}

class LocumVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwGoogleMaps: GMSMapView!
    
    //MARK:- Variable Declration
    
    var clusterManager: GMUClusterManager!
    var proximityDistance : String = "0.0"
    var arrLocumList:[JSON] = []
    var currentLocation = CLLocationCoordinate2D()
    

    //MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGoogleMapCluster()
        appDelegate.setUpQuickLocationUpdate()
        appDelegate.delegate = self
        vwGoogleMaps.isMyLocationEnabled = true
//        vwGoogleMaps.settings.myLocationButton = true
        if dictFilter["location"].stringValue == ""{
            dictFilter["location"].stringValue = "surat"
        }
        
        /*vwGoogleMaps.padding = UIEdgeInsets(top: 0, left: 0, bottom: 300, right: 50)
        
        for object in self.vwGoogleMaps.subviews{
            if(object.theClassName == "GMSUISettingsPaddingView"){
                for view in object.subviews{
                    if(view.theClassName == "GMSUISettingsView"){
                        for btn in view.subviews{
                            if(btn.theClassName == "GMSx_QTMButton"){
                                var frame = btn.frame
                                frame.origin.y = frame.origin.y - 300
                                btn.frame = frame
                                
                            }
                        }
                    }
                }
            }
        }*/
        
        if isComefromPush{
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumListVC") as! LocumListVC
            obj.handlerGetAllLocum = {[weak self] in
                
                if dictFilter["lat"].stringValue != "" && dictFilter["lng"].stringValue != ""{
                    self?.currentLocation = CLLocationCoordinate2D(latitude: dictFilter["lat"].doubleValue, longitude: dictFilter["lng"].doubleValue)
                }
                self?.getAllLocum()
            }
            self.navigationController?.pushViewController(obj, animated: false)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTwoMenu(strTitle: "Locum")        
    }

}


/*public extension NSObject {
    public var theClassName: String {
        return NSStringFromClass(type(of: self))
    }
}*/

//MARK:- Action ZOne

extension LocumVC {
    
    @IBAction func btnFilterAction(_ sender:UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.handlerFilterLocum = {[weak self] dict in
            self?.currentLocation = CLLocationCoordinate2D(latitude: dict["lat"].doubleValue, longitude: dict["lng"].doubleValue)
            self?.getAllLocum()
        }
        self.navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func btnCurrentLocation(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumListVC") as! LocumListVC
        obj.handlerGetAllLocum = {[weak self] in
            
            if dictFilter["lat"].stringValue != "" && dictFilter["lng"].stringValue != ""{
                self?.currentLocation = CLLocationCoordinate2D(latitude: dictFilter["lat"].doubleValue, longitude: dictFilter["lng"].doubleValue)
            }
            self?.getAllLocum()
//           appDelegate.delegate = self
//            appDelegate.setUpQuickLocationUpdate()
        }
        self.navigationController?.pushViewController(obj, animated: false)
        
    }
    
    override func btnAddLocumAction() {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddLocumVC") as! AddLocumVC
        obj.handlerAddLocum = {[weak self] in
            self?.getAllLocum()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnUserCurrentLocation(_ sender:UIButton){
        
        setUpPinOnMap(lat: kUserCurrentLocation.latitude, long: kUserCurrentLocation.longitude)
    }
    
    
}

//MARK:- Setup Pin On google Mapa

extension LocumVC{
    func setUpPinOnMap(lat:Double,long:Double)
    {
        /*let currentLocationMarker = GMSMarker()
        currentLocationMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        currentLocationMarker.icon = UIImage(named: "ic_pin")!.withRenderingMode(.alwaysTemplate)
        currentLocationMarker.map = vwGoogleMaps*/
        
        let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: Float(kGoogleMapZoomingLevel))
       
    }
}

//MARK:- get Current Location Delegate

extension LocumVC:CurrentLocationDelegate {
    func didUpdateLocation(lat: Double?, lon: Double?) {
        
        if lat == nil && lon == nil {
            return
        }
        kUserCurrentLocation = CLLocationCoordinate2D(latitude: lat ?? 20.5937, longitude: lon ?? 78.9629)
        currentLocation = kUserCurrentLocation
//        setUpPinOnMap(lat: kUserCurrentLocation.latitude, long: kUserCurrentLocation.longitude)
        let strLocation = getAddressFromLatLon(latitude: kUserCurrentLocation.latitude, longitude: kUserCurrentLocation.longitude) { (currentAddress) in
        }
        
        setCamera(to: kUserCurrentLocation)
        
        getAllLocum()
    }
}

//MARK:- Google Map Clustring Item

extension LocumVC:GMSMapViewDelegate,GMUClusterManagerDelegate, GMUClusterRendererDelegate{
    
    func setupGoogleMapCluster() {
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: vwGoogleMaps,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: vwGoogleMaps, algorithm: algorithm, renderer: renderer)
        renderer.delegate = self
        vwGoogleMaps.delegate = self
    }
    
    func setCamera(to locValue: CLLocationCoordinate2D) {
//        vwGoogleMaps.isMyLocationEnabled = true
//        vwGoogleMaps.settings.myLocationButton = true
        
        let circle = GMSCircle(position: locValue, radius: (CLLocationDistance((Float(proximityDistance)!/2) * 1000)))
        circle.strokeColor = UIColor.clear
        //circle.fillColor = UIColor(red: 0, green: 0, blue: 0.35, alpha: 0.05)
        circle.fillColor = .clear
        circle.map = vwGoogleMaps
        
//        let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: Float(calculateZoom(radius: Float(proximityDistance)!)))
        let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: kGoogleMapZoomingLevel)
        
        vwGoogleMaps.camera = camera
        
       
    }
    
    //MARK: - Get Zoom Level
    func getZoomLevel(Circle : GMSCircle) -> Int
    {
        let radius = Circle.radius
        let scale = radius / 500
        let zoomLevel = Int((16 - log(scale) / log(2)))
        return zoomLevel
    }
    
    func calculateZoom(radius : Float) -> CGFloat
    {
        //var metersPerPixel = cos(lat * MH_PIE/180) * 2 * MH_PIE * 6378137 / (256 * pow(2, zoom))
        //return round(14-Math.log(radius)/Math.LN2)
        return log2(CGFloat(360 * vwGoogleMaps.bounds.size.width) / CGFloat(radius)) - CGFloat(kGoogleMapZoomingLevel)
        
    }
    
    func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    //MARK: - Show Pin on map
    
    //    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
    //
    //        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
    //                                                 zoom: viewMap.camera.zoom + 1)
    //        let update = GMSCameraUpdate.setCamera(newCamera)
    //        viewMap.moveCamera(update)
    //        return true
    //    }
    //
    
 
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if let data = marker.userData as? POIItem {
            print("index \(data.index)")
            marker.icon = UIImage(named: "ic_pin")!.withRenderingMode(.alwaysTemplate)

        }
    }
    
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        print("tap on cluster \(cluster.items)")
        if cluster.items.count > 1{
            var arrLocumCluster :  [JSON] = []
            for j in 0..<cluster.items.count{
                if let poiItem = cluster.items[j] as? POIItem {
                   arrLocumCluster.append(poiItem.dictData)                    
                }
            }
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumChildListVC") as! LocumChildListVC
            obj.arrClusterLocum = arrLocumCluster
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .overCurrentContext
            obj.handlerLocumDetail = {[weak self] dict in
                let obj = self?.storyboard?.instantiateViewController(withIdentifier: "LocumDetailVC") as! LocumDetailVC
                obj.dictLocumDetail = dict
                obj.handlerRefreshLocum = {[weak self] in
                    self?.getAllLocum()
                }
                self?.navigationController?.pushViewController(obj, animated: true)
            }
            self.present(obj, animated: false, completion: nil)
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        vwGoogleMaps.selectedMarker = nil
        if let poiItem = marker.userData as? POIItem {
            NSLog("Did tap marker for cluster item \(poiItem.dictData)")
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumDetailVC") as! LocumDetailVC
            obj.dictLocumDetail = poiItem.dictData
            obj.handlerRefreshLocum = {[weak self] in
                self?.getAllLocum()
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        return false
    }
    
    func showMultiplePins()
    {
        print("Json array - \(arrLocumList)")
        let extent = 0.2
        for i in 0..<arrLocumList.count {
            let DetailsData = arrLocumList[i]
            let coord = CLLocationCoordinate2D(latitude: CLLocationDegrees(DetailsData["lat"].doubleValue), longitude: CLLocationDegrees(DetailsData["lng"].doubleValue))
            
//            vwGoogleMaps.selectedMarker = nil
            let item = POIItem(position: coord, name: DetailsData["location"].stringValue, dict: DetailsData)
            item.index = i
            clusterManager.add(item)
        }
        clusterManager.cluster()
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    
}

//MARK:- Service

extension LocumVC
{
    func getAllLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kGetLocum)"
            
            print("URL: \(url)")
            
            var param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "lat":"\(currentLocation.latitude)",
                          "lng":"\(currentLocation.longitude)"]
            
            if dictFilter["start_date"].stringValue != "" && dictFilter["start_time"].stringValue != ""{
                let strStartDate = "\(dictFilter["start_date"].stringValue) \(dictFilter["start_time"].stringValue)"
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: strStartDate)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: finalDate)
                param["date_time_from"]  = strFinalFromDate
            } else  if dictFilter["start_time"].stringValue == "" &&  dictFilter["start_date"].stringValue != ""{
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["start_date"].stringValue)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: finalDate)
                param["date_time_from"] = strFinalFromDate + " 00:00:00"
            } else {
                param["date_time_from"] = ""
            }
            
            if dictFilter["to_date"].stringValue != "" && dictFilter["to_time"].stringValue != ""{
                let strStartDate = "\(dictFilter["to_date"].stringValue) \(dictFilter["to_time"].stringValue)"
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: strStartDate)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: finalDate)
                param["date_time_to"]  = strFinalFromDate
            } else  if dictFilter["to_time"].stringValue == "" && dictFilter["to_date"].stringValue != ""{
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["start_date"].stringValue)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: finalDate)
                param["date_time_to"] = strFinalFromDate + " 00:00:00"
            } else {
                param["date_time_to"] = ""
            }
            
            param["rate_hr"] = dictFilter["rate_hr"].stringValue
            param["location"]  = dictFilter["location"].stringValue
            
            if currentLocation.latitude == 0.0 && currentLocation.longitude == 0.0{
                param["lat"] = "\(kUserCurrentLocation.latitude)"
                param["lng"] = "\(kUserCurrentLocation.longitude)"
            }
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.arrLocumList = []
                        self.arrLocumList = json["data"].arrayValue
                        self.clusterManager.clearItems()
                        self.showMultiplePins()
                        if self.currentLocation.latitude == 0.0 && self.currentLocation.longitude == 0.0{
                            self.setCamera(to: kUserCurrentLocation)
                        } else {
                            self.setCamera(to: self.currentLocation)
                        }
                    }
                    else
                    {
                        self.arrLocumList = []
                        self.arrLocumList = json["data"].arrayValue
                        self.clusterManager.clearItems()
                        self.vwGoogleMaps.clear()
                        if self.currentLocation.latitude == 0.0 && self.currentLocation.longitude == 0.0{
                            self.setCamera(to: kUserCurrentLocation)
                        } else {
                            self.setCamera(to: self.currentLocation)
                        }
//                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}
