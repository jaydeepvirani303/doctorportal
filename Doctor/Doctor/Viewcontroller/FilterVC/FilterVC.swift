//
//  FilterVC.swift
//  Doctor
//
//  Created by Jaydeep on 03/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import SwiftyJSON


class FilterVC: UIViewController {
    
    //MARK:- Variable Declration
    
    var locationUser = CLLocationCoordinate2D()
    var handlerFilterLocum:(JSON) -> Void = {_ in }
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtvwLocation: UITextView!
    @IBOutlet weak var txtRate: CustomTextField!
    @IBOutlet weak var txtStartDate: CustomTextField!
    @IBOutlet weak var txtStartTime: CustomTextField!
    @IBOutlet weak var txtToDate: CustomTextField!
    @IBOutlet weak var txtToTime: CustomTextField!
    @IBOutlet weak var lblCurrency: UILabel!
    
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithPresent(strTitle: "Filter")
    }
    
    override func btnClearFilterAction(_ sender: UIButton) {
        dictFilter = JSON()
        dictFilter["location"].stringValue = "surat"
        setupFilterData()
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
       
        self.txtvwLocation.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        appDelegate.delegate = self
        
        [txtStartDate,txtStartTime,txtToDate,txtToTime].forEach { (txtField) in
            txtField?.delegate = self
        }
        
        setupFilterData()
        
        lblCurrency.text = getCurrencyCode()
        addDoneButtonOnKeyboard(textfield: txtRate)
    }
    
    func setupFilterData() {
        self.txtvwLocation.text = dictFilter["location"].stringValue == "surat" ? "" : dictFilter["location"].stringValue
        self.txtRate.text = dictFilter["rate_hr"].stringValue
        self.lblPlaceholder.isHidden = self.txtvwLocation.text.isEmpty ? false : true
        locationUser = CLLocationCoordinate2D(latitude: dictFilter["lat"].doubleValue, longitude: dictFilter["lng"].doubleValue)
        if dictFilter["start_date"].stringValue != ""{
            let date = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["start_date"].stringValue)
            self.txtStartDate.text = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: date)
        } else {
            self.txtStartDate.text = ""
        }
        if dictFilter["start_time"].stringValue != ""{
            let date = StringToDate(Formatter: dateFormatter.dateFormate3.rawValue, strDate: dictFilter["start_time"].stringValue)
            self.txtStartTime.text = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: date)
        } else {
            self.txtStartTime.text = ""
        }
        
        if dictFilter["to_date"].stringValue != ""{
            let date = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["to_date"].stringValue)
            self.txtToDate.text = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: date)
        } else {
            self.txtToDate.text = ""
        }
        
        if dictFilter["to_time"].stringValue != ""{
            let date = StringToDate(Formatter: dateFormatter.dateFormate3.rawValue, strDate: dictFilter["to_time"].stringValue)
            self.txtToTime.text = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: date)
        } else {
            self.txtToTime.text = ""
        }
    }
    //MARK:- Action Zone
    
    @IBAction func btnCurrentLocation(_ sender:UIButton){
        appDelegate.setUpQuickLocationUpdate()
    }
    
    @IBAction func btnApplyAction(_ sender:UIButton){
        dictFilter["start_date"].stringValue = txtStartDate.text ?? ""
        dictFilter["start_time"].stringValue = txtStartTime.text ?? ""
        dictFilter["to_date"].stringValue = txtToDate.text ?? ""
        dictFilter["to_time"].stringValue = txtToTime.text ?? ""
        dictFilter["location"].stringValue = txtvwLocation.text == "" ? "surat" : txtvwLocation.text!
        dictFilter["lat"].doubleValue = locationUser.latitude
        dictFilter["lng"].doubleValue = locationUser.longitude
        dictFilter["rate_hr"].stringValue = txtRate.text ?? ""
        handlerFilterLocum(dictFilter)
        self.navigationController?.popViewController(animated: false)
    }

}

//MARK:- get Current Location Delegate

extension FilterVC:CurrentLocationDelegate {
    func didUpdateLocation(lat: Double?, lon: Double?) {
        
        if lat == nil && lon == nil {
            return
        }
        locationUser = CLLocationCoordinate2D(latitude: lat ?? 20.5937, longitude: lon ?? 78.9629)
        //        showLoader()
        let strLocation = getAddressFromLatLon(latitude: locationUser.latitude, longitude: locationUser.longitude) { (currentAddress) in
            self.stopLoader()
            self.txtvwLocation.text = currentAddress
            self.lblPlaceholder.isHidden = self.txtvwLocation.text.isEmpty ? false : true
        }
    }
}


//MARK:- UITextview Delegate

extension FilterVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.lblPlaceholder.isHidden = false
        } else {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        setupGooglePlacePicker()
        return false
    }
}

//MARK:- UITextfield Delegate
extension FilterVC:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtStartDate {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.type = 0
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = true
            obj.isSetDate = true
            
            obj.setDateValue = Date()
            obj.minimumDate = Date()
            
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        } else if textField == txtStartTime {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = false
            obj.isSetDate = false
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
            obj.type = 1
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        } else if textField == txtToDate {
            
            if txtStartDate.text == "" {
                makeToast(message: "Please select start date")
                return false
            }
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.type = 2
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = true
            obj.isSetDate = true
            
            obj.setDateValue = Date()
            obj.minimumDate = StringToDate(Formatter: "dd/MM/yyyy", strDate: txtStartDate.text!)
            
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        } else if textField == txtToTime {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = false
            obj.isSetDate = false
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
            obj.type = 3
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}

extension FilterVC : datePickerDelegate
{
    func setDateValue(dateValue: Date,type:Int) {
        if type == 0 {
            let strDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: dateValue)
            self.txtStartDate.text = strDate
        } else if type == 1 {
            let strTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: dateValue)
            self.txtStartTime.text = strTime
        } else if type == 2 {
            let strDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: dateValue)
            self.txtToDate.text = strDate
        } else if type == 3 {
            let strTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: dateValue)
            self.txtToTime.text = strTime
        }
    }
}

//MARK:- Google Place Picker

extension FilterVC:GMSAutocompleteViewControllerDelegate {
    
    func setupGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.all.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
         filter.type = .city
         autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress)")
        
        locationUser = place.coordinate
        
        self.txtvwLocation.text = place.formattedAddress ?? ""
        self.lblPlaceholder.isHidden = self.txtvwLocation.text == "" ? false : true
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
