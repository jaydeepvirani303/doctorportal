//
//  ViewController.swift
//  Doctor
//
//  Created by Jaydeep on 30/06/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseMessaging

class ViewController: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictSocialData = JSON()
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var btnDoctorOutlet: UIButton!
    @IBOutlet weak var btnClinicOutlet: UIButton!    
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRemberOutlet: UIButton!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupGoogleSignIn()
        
        if getUserDetail("dc_user_id") != ""{
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
            self.navigateUserWithLG(obj: obj)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        checkUserSession()
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
        self.btnDoctorAction(self.btnDoctorOutlet)
        [txtEmailAddress,txtPassword].forEach { (txtField) in
            txtField?.delegate = self
        }
    }
    
    func setupGoogleSignIn(){
        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = kGoogleSignKey
        
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
    }
    
    func checkUserSession(){
        
        btnRemberOutlet.isSelected = false
        
        guard let email = Defaults.value(forKey: "userEmail") else { return }
        guard let password = Defaults.value(forKey: "userPassword") else { return }
        btnRemberOutlet.isSelected = true
        self.txtEmailAddress.text = "\(email)"
        self.txtPassword.text = "\(password)"
    }

}

//MARK:- Textfield methods
extension ViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {        
        textField.resignFirstResponder()
        return true
    }
}


//MARK:- Action Zone

extension ViewController {
    @IBAction func btnLoginAction(_ sender:UIButton){
        
        objUser = User()
        objUser.strEmailAddress = self.txtEmailAddress.text ?? ""
        objUser.strPassword = self.txtPassword.text ?? ""
        
        if objUser.isLogin()
        {
            userLogin()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
        
    }
    
    @IBAction func btnSignupAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnForgotPasswordAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnRememberAction(_ sender:UIButton){
        btnRemberOutlet.isSelected = !btnRemberOutlet.isSelected
    }
    
    @IBAction func btnGoogleLoginAction(_ sender:UIButton){
        loadGoogleUserData()
    }
    
    @IBAction func btnFacebookLoginAction(_ sender:UIButton){
        fbLoginAction()
    }
}

//MARK:- Action Zone

extension ViewController {
    
    @IBAction func btnDoctorAction(_ sender:UIButton){
        self.btnClinicOutlet.isSelected = false
        self.btnDoctorOutlet.isSelected = true
    }
    
    @IBAction func btnClinicAction(_ sender:UIButton){
        self.btnClinicOutlet.isSelected = true
        self.btnDoctorOutlet.isSelected = false
    }
}

//MARK: - Google Login related
extension ViewController: GIDSignInDelegate {
    
    func loadGoogleUserData() {
        let signin = GIDSignIn.sharedInstance()
        signin?.scopes = ["https://www.googleapis.com/auth/userinfo.profile","https://www.googleapis.com/auth/user.birthday.read"]
        signin?.signOut()
        signin?.signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        print(error)
        
        if error == nil {
            print("Google Signin Success")
            print(user.userID)
            print(user.profile.name)
            print(user.profile.email)
            print(user.profile.imageURL(withDimension: 500))
            
            var dict = JSON()
            dict["id"] = JSON(user.userID)
            dict["name"] = JSON(user.profile.name)
            dict["email"] = JSON(user.profile.email)
            dict["profile_image"] = JSON(user.profile.imageURL(withDimension: 500))
            dict["social_type"]  = "Google"
            self.isCheckSocialLogin(dict: dict)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
}

//MARK:- Fb Login

extension ViewController
{
    func fbLoginAction()
    {

        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["email","public_profile"], from: self, handler: { (result, error) -> Void in
            if (error == nil)
            {
                print(result!)
                if (result?.grantedPermissions != nil)
                {
                    self.returnUserData()
                }
                else
                {
                    self.showAlert(strMsg: result?.grantedPermissions.description ?? "Something went wrong")
                    self.stopLoader()
                }
            }
            else
            {
                self.showAlert(strMsg: error.debugDescription + "Log in error")
                self.stopLoader()
            }
        })
    }
    
    func returnUserData()
    {
        if((AccessToken.current) != nil)
        {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                print("FBLoginData",JSON(result as! NSDictionary))
                if (error == nil)
                {
                    var dict = JSON(result as! NSDictionary)
                    dict["profile_image"]  = JSON(dict["picture"]["data"]["url"].stringValue)
                    dict["social_type"]  = "Facebook"
                    self.dictSocialData = dict
                    self.perform(#selector(self.redirectFBLogin), with: nil, afterDelay: 2)
                } else {
                    self.showAlert(strMsg: error.debugDescription + "Graph request error")
                }
            })
        }else{
            self.showAlert(strMsg: "No Access Token")
        }
    }
    
    @objc func redirectFBLogin(){
        self.isCheckSocialLogin(dict: dictSocialData)
    }
    
    
    func showAlert(strMsg:String) {
        makeToast(message: strMsg)
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: "Facebook Error", message: strMsg, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
        
    }
}




//MARK:- Service

extension ViewController
{
    func userLogin()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kLogin)"
            
            print("URL: \(url)")
            
            let param =  ["email_id":objUser.strEmailAddress,
                          "password":objUser.strPassword,
                          "device_token":Messaging.messaging().fcmToken ?? "",
                          "type":self.btnDoctorOutlet.isSelected == true ? "Doctor" : "Clinic",
                          "device_type":strDeviceType]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        if self.btnRemberOutlet.isSelected{
                            Defaults.set("\(self.txtEmailAddress.text!)", forKey: "userEmail")
                            Defaults.set("\(self.txtPassword.text!)", forKey: "userPassword")
                        } else {
                            Defaults.removeObject(forKey: "userEmail")
                            Defaults.removeObject(forKey: "userPassword")
                        }
                        
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
                        self.navigateUserWithLG(obj: obj)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func isCheckSocialLogin(dict:JSON)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            var url = String()
            url = "\(kBasicURL)\(kSocialAccount)"
            
            print("URL: \(url)")
            
            var param =  ["social_id":dict["id"].stringValue,
                          "device_type": strDeviceType
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        let data = json["data"]
                        if data == JSON.null {
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
                            obj.dictData = dict
                            obj.isComeFromSocial = true
                            self.navigationController?.pushViewController(obj, animated: true)
                        } else {
                            guard let rowdata = try? data.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "userDetail")
                            Defaults.synchronize()
                            
                            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
                            self.navigateUserWithLG(obj: obj)
                        }
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong + "it is json problem")
                }
            }
            
        }
        else
        {
            self.showAlert(strMsg: kNoInternetConnection)
        }
    }
    
    
}

