//
//  LocumCell.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import HCSStarRatingView

class LocumCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnRedirectionOutlet: UIButton!
    @IBOutlet weak var vwIndicator: UIView!
    
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblHospitalAddress: UILabel!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var vwAvgRate: HCSStarRatingView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwStatus: CustomView!
    
    
    
    //MARK:- View Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        contentView.layoutIfNeeded()
        contentView.layoutSubviews()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        vwIndicator.roundCorners([.topLeft,.bottomLeft], radius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
