//
//  User.swift
//  Doctor
//
//  Created by Jaydeep on 22/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

class User: NSObject {
    
    //MARK:- Login
    var strEmailAddress:String = ""
    var strPassword:String = ""
    
    //MARK:- Registration
    
    var strFullName:String = ""
    var strClinicName:String = ""
    var strClinicAddress:String = ""
    var strMobileNumber:String = ""
    var strRegistrationNo:String = ""
    var strConfirmPassword:String = ""
    var strCountryCode:String = ""
    
    //MARK:- Add Locum
    
    var strLocumRate:String = ""
    var strLocumTotalHr:String = ""
    var strLocumStartDate:String = ""
    var strLocumStartTime:String = ""
    var strLocumComment:String = ""
    
    //MARK:- Change Password
    
    var strOldPassword:String = ""
    var strNewPassword:String = ""
    
    //MARK:- Update Profile
    
    var isImage:Bool = false
    var strBio:String = ""
    
    //MARK:- Return Validation message
    
    var strValidationMessage : String = ""
    
    //MARK:- Login Method
    func isLogin() -> Bool {
        
        if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }
        else if (strPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter password"
            return false
        }
        return true
    }
    
    //MARK:- Registration Method
    func isClinicRegistration() -> Bool {
        
        if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter full name"
            return false
        }
        else if (strClinicName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter clinic name"
            return false
        }
        else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter country code"
            return false
        }
        else if strCountryCode.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid country code"
            return false
        }
        else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter mobile number"
            return false
        }
        else if strMobileNumber.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        }
        else if (strClinicAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter clinic address"
            return false
        }
        else if (strRegistrationNo.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter registration no"
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }
        else if (strPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter your password"
            return false
        }
        else if strPassword.count < 8
        {
            self.strValidationMessage = "Password must be at least 8 characters long"
            return false
        }
        else if(strPassword.isStrongPassword(strPassword) == false){
            self.strValidationMessage = "Please enter password minimum 8 characters at least 1 alphabet and 1 number."
            return false
        }
        else if strConfirmPassword == ""
        {
            self.strValidationMessage = "Please enter confirm password"
            return false
        }
        else if(!(strPassword == strConfirmPassword))
        {
            self.strValidationMessage = "Password and confirmation password must match"
            return false
        }
        
        return true
    }
    
    func isDoctorRegistration() -> Bool {
        
        if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter full name"
            return false
        }
        else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter country code"
            return false
        }
        else if strCountryCode.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid country code"
            return false
        }
        else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter mobile number"
            return false
        }
        else if strMobileNumber.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        }
        else if (strRegistrationNo.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter registration no"
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }
        else if (strPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter your password"
            return false
        }
        else if strPassword.count < 8
        {
            self.strValidationMessage = "Password must be at least 8 characters long"
            return false
        }
        else if(strPassword.isStrongPassword(strPassword) == false){
            self.strValidationMessage = "Please enter password minimum 8 characters at least 1 alphabet and 1 number."
            return false
        }
        else if strConfirmPassword == ""
        {
            self.strValidationMessage = "Please enter confirm password"
            return false
        }
        else if(!(strPassword == strConfirmPassword))
        {
            self.strValidationMessage = "Password and confirmation password must match"
            return false
        }
        return true
    }
    
    func isForgotPassword() -> Bool {
        
        if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }
        return true
    }
    
    func isDoctorRegistrationWithSocialAccount() -> Bool {
        
        if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter full name"
            return false
        }
        else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter country code"
            return false
        }
        else if strCountryCode.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid country code"
            return false
        }
        else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter mobile number"
            return false
        }
        else if strMobileNumber.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        }
        else if (strRegistrationNo.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter registration no"
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }
        
        return true
    }
    
    func isClinicRegistrationWithSocialAccount() -> Bool {
        
        if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter full name"
            return false
        }
        else if (strClinicName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter clinic name"
            return false
        }
        else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter country code"
            return false
        }
        else if strCountryCode.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid country code"
            return false
        }
        else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter mobile number"
            return false
        }
        else if strMobileNumber.isNumeric == false
        {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        }
        else if (strClinicAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter clinic address"
            return false
        }
        else if (strRegistrationNo.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter registration no"
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter email address"
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = "Please enter valid email address"
            return false
        }        
        
        return true
    }
    
    //MARK:- Add Locum
    
    func isAddNewLocum() -> Bool {
        if (strClinicName.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter Clinic/Hospital name"
            return false
        } else if (strClinicAddress.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please select location"
            return false
        } else if (strLocumRate.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter rate per hour"
            return false
        } else if (strLocumTotalHr.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter total hour(s)"
            return false
        } else if (strLocumStartDate.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please select from date"
            return false
        } else if (strLocumStartTime.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please select from time"
            return false
        }
        return true
    }
    
    //MARK:- Change Password
    
    func isChangePassword() -> Bool {
        if (strOldPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = "Please enter your old password"
            return false
        }
        else if strNewPassword.count < 8
        {
            self.strValidationMessage = "Your password must be at least 8 characters long"
            return false
        }
        else if(strNewPassword.isStrongPassword(strNewPassword) == false){
            self.strValidationMessage = "Please enter new password minimum 8 characters at least 1 alphabet and 1 number."
            return false
        }
        else if strConfirmPassword == ""
        {
            self.strValidationMessage = "Please enter confirm password"
            return false
        }
        else if(!(strNewPassword == strConfirmPassword))
        {
            self.strValidationMessage = "New password and confirmation password must match"
            return false
        }
        return true
    }
    
    //MARK:- Update Profile
    
    func isUpdateDoctorProfile() -> Bool {
        if isImage == false {
            self.strValidationMessage = "Please select profile picture"
            return false
        } else if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter full name"
            return false
        } else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter country code"
            return false
        } else if strCountryCode.isNumeric == false {
            self.strValidationMessage = "Please enter valid country code"
            return false
        } else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter mobile number"
            return false
        } else if strMobileNumber.isNumeric == false {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        } else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter email address"
            return false
        } else if strEmailAddress.isValidEmail() == false {
            self.strValidationMessage = "Please enter valid email address"
            return false
        } else if (strRegistrationNo.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter year of experience"
            return false
        } else if (strBio.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter more about yourself"
            return false
        }
        return true
    }
    
    func isUpdateClinicProfile() -> Bool {
        if isImage == false {
            self.strValidationMessage = "Please select profile picture"
            return false
        } else if (strFullName.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter full name"
            return false
        } else if (strCountryCode.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter country code"
            return false
        } else if strCountryCode.isNumeric == false {
            self.strValidationMessage = "Please enter valid country code"
            return false
        } else if (strMobileNumber.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter mobile number"
            return false
        } else if strMobileNumber.isNumeric == false {
            self.strValidationMessage = "Please enter valid mobile number"
            return false
        } else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter email address"
            return false
        } else if strEmailAddress.isValidEmail() == false {
            self.strValidationMessage = "Please enter valid email address"
            return false
        } else if (strClinicName.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter clinic name"
            return false
        } else if (strClinicAddress.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter clinic address"
            return false
        } else if (strBio.trimmingCharacters(in: .whitespaces)).isEmpty {
            self.strValidationMessage = "Please enter more about yourself or your clinic/hospital"
            return false
        }
        return true
    }
    
}
