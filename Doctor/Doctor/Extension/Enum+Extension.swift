//
//  Enum+Extension.swift
//  Doctor
//
//  Created by Jaydeep on 22/08/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

enum dateFormatter:String {
    case dateFormate1 = "dd MMM yyyy"
    case dateFormate2 = "hh:mm a"
    case dateFormate3 = "HH:mm:ss"
    case dateFormate4 = "yyyy-MM-dd HH:mm:ss"
    case dateFormate5 = "dd MMM yyyy hh:mm a"
    case dateFormate6 = "yyyy-MM-dd"
    case dateFormate7 = "dd/MM/yyyy"
    case dateFormate8 = "dd MMM yyyy HH:mm:ss"
}

enum checkDoctorClinic:String{
    case doctor = "Doctor"
    case clinic = "Clinic/Hospital"
}

enum checkAddEditLocum {
    case add
    case edit
}
