//
//  WebServicePrefix.swift
//  Doctor
//
//  Created by Jaydeep on 22/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit


//MARK: - UserName /Password

let basic_username = ""

let basic_password = ""

let kBasicURL = "http://lallanour.com.my/locum/api/"

let kSomethingWentWrong = "Something went wrong"

let kNoInternetConnection = "No internet connection,Please try again later"

//MARK:- API

var kLogin = "login"

var kRegistration = "registration"

var kSocialAccount = "socialaccount"

var kForgotPassword = "forget"

var kAddLocum = "newlocum"

var kGetLocum = "locumlist"

var kApplyLocum = "locum_apply"

var kLocumCancel = "locum_cancel"

var kEditLocum = "editlocum"

var kDeleteLocum = "deletelocum"

var kLocumAppicant = "locum_applicants"

var kLocumHire = "locum_hire"

var kGetProfile = "get_profile"

var kChangePassword = "change_password"

var kUpdateProfile = "update_profile"

var kCalendarList = "locum_list_calendar"

var kAllRating = "all_rating"
