//
//  CommonPickerVC.swift
//  Muber
//
//  Created by Jaydeep on 26/03/18.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

enum selectDropdown
{
    case gender
    case qualification
    case stateList
    case timeZonelist
    case addcard
    case reportSpam
}

protocol CommonPickerDelegate
{
    func setValuePicker(selectedDict:JSON)
}


class CommonPickerVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet var pickerVw: UIPickerView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var vwBtns: UIView!
    
    //MARK: - Variable
    
    var strValueStore = String()
    var arrayPicker: [JSON] = []
    var pickerDelegate : CommonPickerDelegate?
    var selectedYear = String()
    var selectedMonth = String()
    var isComeFromExpiryMonth = Bool()
    var strType = ""
    var selectedDropdown = selectDropdown.gender
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Array : \(self.arrayPicker)")
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.white, for: .normal)
            $0?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        })
        
        btnDone.setTitle("DONE".uppercased(), for: .normal)
        btnCancel.setTitle("CANCEL", for: .normal)
        
        vwBtns.backgroundColor = UIColor.appThemeBlue
        
    }
    
}

extension CommonPickerVC
{

    //MARK: - IBAction
    
    @IBAction func btnCancelTapped(sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnDoneTapped(sender: UIButton)
    {
        let dict = arrayPicker[self.pickerVw.selectedRow(inComponent: 0)]
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setValuePicker(selectedDict: dict)
    }
    
}

extension CommonPickerVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {        
        return arrayPicker[row]["value"].stringValue
    }
}






